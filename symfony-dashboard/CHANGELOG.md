# v.0.0.5 - Public site

- php bin/console make:controller (Page)

# v.0.0.4 - Login

- php bin/console make:factory
- php bin/console make:admin:crud
- UPDATE `symfony-dashboard`.`user` SET `roles`='["ROLE_ADMIN"]' WHERE  `id`=1;
- /register
- php bin/console make:registration-form
- php bin/console make:auth
- php bin/console doctrine:migrations:migrate
- php bin/console make:migration
- php bin/console make:user

# v.0.0.3 - Factories

- php bin/console doctrine:migrations:migrate
- php bin/console make:migration
- UniqueEntity
- php bin/console doctrine:fixtures:load
- AppFixtures
- composer require doctrine/doctrine-fixtures-bundle --dev
- php bin/console make:factory
- composer require zenstruck/foundry --dev

# v.0.0.2 - Easy admin bundle

- php bin/console make:admin:crud
- composer require symfony/apache-pack
- php bin/console debug:router
- php bin/console make:admin:dashboard
- composer require easycorp/easyadmin-bundle

# v.0.0.1 - Initial structure

- php bin/console doctrine:migrations:migrate
- php bin/console make:migration
- php bin/console make:entity (Comment)
- php bin/console make:entity (Post)
- php bin/console make:entity (Category)
- php bin/console doctrine:database:create
- composer require symfony/maker-bundle --dev
- composer require symfony/debug-pack --dev
- composer require symfony/twig-pack
- composer require symfony/orm-pack
- composer create-project symfony/skeleton:"6.3.*" symfony-dashboard