<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function home(PostRepository $postRepository): Response
    {
        return $this->render('page/index.html.twig', [
            'posts' => $postRepository->findLatest(),
        ]);
    }

    #[Route('/blog/{slug}', name: 'app_post')]
    public function blog(Post $post): Response
    {
        return $this->render('page/post.html.twig', [
            'post' => $post,
        ]);
    }
}
