<?php

namespace App\DataFixtures;

use App\Factory\CategoryFactory;
use App\Factory\CommentFactory;
use App\Factory\PostFactory;
use App\Factory\UserFactory;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createOne([
            'email' => 'admin@test.test',
            'roles' => ['ROLE_ADMIN']
        ]);
        CategoryFactory::createMany(8);
        PostFactory::createMany(20, function() {
            return [
                'comments' => CommentFactory::new()->many(0, 3),
                'category' => CategoryFactory::random()
            ];
        });
    }
}
